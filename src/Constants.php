<?php


class Constants
{
    const USER = array(
        'USER_LOGIN' => 'vladglu123@gmail.com', //логин (электронная почта)
        'USER_HASH' => '8b9fd0f2b8b932555945da16851efddaad9083eb' //Хэш для доступа к API
    );

    const  SUBDOMAIN = 'vladglu123'; //Наш аккаунт - поддомен
    const  PHONEFIELIDId = '44000';//ID поля "Телефон"
    const  EMAILFIELDId = '4409'; //ID поля "Email"
    const  RESPONSIBLEId = '2614519'; //ID Ответственного сотрудника

    const DealName = 'Заявка с сайта'; //Название создаваемой сделки
    const DealStatusID = '20476129'; //ID статуса сделки
    const DealSale = '70000'; //Сумма сделки
    const DealTags = 'Сделка';  //Теги для сделки
    const ContactTags = 'Контакт'; //Теги для контакта

}