<?php
include 'AmoAPI.php';


/**
 * Класс реализующий бизнес-логику
 */
class Controller
{


    private $api;

    public function __construct()
    {
        $this->api = new AmoAPI();

    }

    /**
     * Если мы успешно авторизовались, то ищем существующий контакт с email и phone из формы.
     * Если контакт существует, создается новая сделка, контакт привязывается помимо своих сделок в новую
     * Если контакта не существует, просто создается новый контакт с данными из формы.
     *
     * @param $subdomain -Наш аккаунт - поддомен
     * @param $user
     * @param $email
     * @param $phone
     * @param $responsibleId
     * @param $dealName -Название создаваемой сделки
     * @param $dealStatusID
     * @param $dealSale -Сумма сделки
     * @param $dealTags
     * @param $name
     * @param $phoneFieldId
     * @param $emailFieldId
     * @param $contactTags
     */
    public function create($user, $subdomain, $email, $phone, $responsibleId, $dealName, $dealStatusID, $dealSale, $dealTags, $name, $phoneFieldId, $emailFieldId, $contactTags)
    {
        $api = $this->api;
        if ($api->authorize($user, $subdomain) == true) {
            $contactInfo = $api->findContact($subdomain, $email, $phone);
            $idContact = $contactInfo['idContact'];
            if ($idContact != null) {
                $task = $api->task($idDeal, $responsibleId, $subdomain);
                $idDeal[] = $api->addDeal($dealName, $dealStatusID, $dealSale, $responsibleId, $dealTags, $subdomain);
                if ($idDeal != null) {
                    if (!empty($contactInfo['idLeads'])) {
                        foreach ($contactInfo['idLeads'] as $idLeads) {
                            $idDeal[] = $idLeads;
                        }
                    }
                    $api->editContact($idContact, $idDeal, $subdomain);
                }
            } else {
                $api->addContact($name, $responsibleId, $phoneFieldId, $phone, $emailFieldId, $email, $subdomain, $contactTags);
            }
        }
    }

    /**
     * Отправка почты администратору
     * @param $from -почта отправтеля
     */
    public  function sendEmail($from){
        /*
        $to = "vladglu123@gmail.com";
        $subject = "Заявка с сайта";
        $txt = "Hello world!";
        $headers = "From: "+$from . "\r\n" .
            "CC: "+$to;

        mail($to,$subject,$txt,$headers);*/

        ini_set( 'display_errors', 1 );
        error_reporting( E_ALL );
        //$from = "test@hostinger-tutorials.com";
        $to = "vladglu123@gmail.com";
        $subject = "Заявка с сайта";
        $message = "Новая заявка";
        $headers = "From:" . $from;
        mail($to,$subject,$message, $headers);
        echo "The email message was sent.";
    }

}

