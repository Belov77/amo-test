var allFields = [
    '#name',
    '#phone',
    '#email'
];

function checkForSubmit() {
    for (var i = 0; i < allFields.length; i++) {
        if ($(allFields[i]).hasClass('correct')) {
            $('#submit').removeAttr('disabled');
            $('#submit').css({
                "border": "0",
                "background-color": "greenyellow",
                "color": "white"
            });
        } else {
            $("#submit").attr("disabled", true);
            $('#submit').css({
                "background-color": "#c62b4b",
                "color": "black"
            });
        }
    }
}

function correct(field, error) {
    $(field).removeClass("incorrect");
    $(field).addClass("correct");
    $(error).css({"display": "none"});
}

function incorrect(field, error) {
    $(field).removeClass("correct");
    $(field).addClass("incorrect");
    $(error).css({"display": "inline"});
}




function checkName() {
    var regexp = /^[А-Яа-я]+$/;
    $('#name').change(function () {
        if (!($(this).val().match(regexp))) {
            incorrect('#firstname', '#fir_er');
        } else {
            correct('#firstname', '#fir_er');
        }
    });
}

function checkEmail() {
    var regexp = /^[a-z0-9]+@[a-z0-9]+\.[a-z]+$/;
    $('#email').change(function () {
        if (!($(this).val().match(regexp))) {
            incorrect('#email', '#email_er');
        } else {
            correct('#email', '#email_er');
        }
        checkForSubmit();
    });
}

function checkPhone() {
    var regexp = /^[0-9]{5,20}$/;
    $('#phone').change(function () {
        if (!($(this).val().match(regexp))) {
            incorrect('#email', '#email_er');
        } else {
            correct('#email', '#email_er');
        }
        checkForSubmit();
    });
}

$(function () {
     checkPhone(), checkEmail();

    $("#my_form").submit(function (event) {
        event.preventDefault(); //prevent default action
        var post_url = $(this).attr("action"); //get form action url
        var request_method = $(this).attr("method"); //get form GET/POST method
        var form_data = $(this).serialize(); //Encode form elements for submission

        $.ajax({
            url: post_url,
            type: request_method,
            data: form_data
        }).done(function (response) { //
            $("#server-results").html(response);
        });
    });
});